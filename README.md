# OpenML dataset: Meta_Album_PLT_VIL_Mini

https://www.openml.org/d/44286

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album Plant Village Dataset (Mini)**
***
The Plant Village dataset(https://data.mendeley.com/datasets/tywbtsjrjv/1) contains camera photos of 17 crop leaves. The original image resolution is 256x256 px. This collection covers 26 plant diseases and 12 healthy plants. The leaves are removed from the plant and placed on gray or black background, in various lighting conditions. All images are captured on a variety of gray backgrounds, except Corn Common rust which has a black background. For the curated version, we exclude the irrelevant Background and Corn Common Rust classes from the original collection. Plant Village has a 2-level label hierarchy, the supercategory is the crop type and the category is the disease type. We have preprocessed Plant Village for Meta-Album by resizing a subset from the original dataset to 128x128 image size.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/PLT_VIL.png)

**Meta Album ID**: PLT_DIS.PLT_VIL  
**Meta Album URL**: [https://meta-album.github.io/datasets/PLT_VIL.html](https://meta-album.github.io/datasets/PLT_VIL.html)  
**Domain ID**: PLT_DIS  
**Domain Name**: Plant Diseases  
**Dataset ID**: PLT_VIL  
**Dataset Name**: Plant Village  
**Short Description**: Plant disease dataset  
**\# Classes**: 38  
**\# Images**: 1520  
**Keywords**: plants, plant diseases  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: CC0 1.0  
**License URL(original data release)**: https://data.mendeley.com/datasets/tywbtsjrjv/1
https://creativecommons.org/publicdomain/zero/1.0/
 
**License (Meta-Album data release)**: CC0 1.0  
**License URL (Meta-Album data release)**: [https://creativecommons.org/publicdomain/zero/1.0/](https://creativecommons.org/publicdomain/zero/1.0/)  

**Source**: Plant Village Dataset  
**Source URL**: https://github.com/spMohanty/PlantVillage-Dataset  
  
**Original Author**: Sharada Mohanty, David Hughes, and Marcel Salathe  
**Original contact**: arunpandian@mamcet.com  

**Meta Album author**: Phan Anh VU  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@article{G2019323,
    title = {Identification of plant leaf diseases using a nine-layer deep convolutional neural network},
    journal = {Computers & Electrical Engineering},
    volume = {76},
    pages = {323-338},
    year = {2019},
    issn = {0045-7906},
    doi = {https://doi.org/10.1016/j.compeleceng.2019.04.011},
    url = {https://www.sciencedirect.com/science/article/pii/S0045790619300023},
    author = {Geetharamani G. and Arun Pandian J.},
    keywords = {Artificial intelligence, Deep convolutional neural networks, Deep learning, Dropout, Image augmentation, Leaf diseases identification, Machine learning, Mini batch, Training epoch, Transfer learning},
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Micro]](https://www.openml.org/d/44242)  [[Extended]](https://www.openml.org/d/44321)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44286) of an [OpenML dataset](https://www.openml.org/d/44286). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44286/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44286/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44286/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

